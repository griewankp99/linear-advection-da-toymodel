#!/usr/bin/env python

# Collection of various plot scripts. 
# All plot scripts return the figure and axes object. Final tweaks and saving are intended to happen in notebooks
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np

from LinAdvFunc.da_functions import *

def ensemble_plotter(states,m_const,da_const,j=0,t_start=0,t_end=0):
    """
    Plots the full forecast/background ensemble with observations, as well as the resulting analysis.
    Plots all timesteps from t_start till t_end. 
    Initial starting conditions have no background state
    
    
    ToDo: 
    intital state needs to be added as an exception with no backbround state. 
 
    Input: 
    j : experiment number
    t_start and t_end: time frame to plot

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')
    nc = da_const['ncyc']
    if t_end ==0: t_end = nc

    na = t_end-t_start

    alpha = np.sqrt(1/da_const['nens'])
    
    fig, ax = plt.subplots(na,2,figsize=(7.5,na*1.5),sharex='all',sharey='all')
  
    #left bg, right analysis
    
    
    #for c in range(nc):
    for c in range(na):
        for i in range(da_const["nens"]):
            ax[c,0].plot(m_const['x_grid'],states[j]['bg'][c+t_start][:,i],'r',alpha =alpha,zorder=1)
            ax[c,1].plot(m_const['x_grid'],states[j]['an'][c+t_start][:,i],'magenta',alpha =alpha,zorder=1)
        ax[c,0].plot(m_const['x_grid'],np.mean(states[j]['bg'][c+t_start][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
        ax[c,0].plot(m_const["x_grid"],states[j]['truth'][c+t_start],'k',zorder=10,label='truth')
        
        ax[c,1].plot(m_const['x_grid'],np.mean(states[j]['an'][c+t_start][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
        ax[c,1].plot(m_const["x_grid"],states[j]['truth'][c+t_start],'k',zorder=10,label='truth')
        
        ax[c,0].scatter(m_const["x_grid"][da_const["obs_loc"]],states[j]['obs'][c+t_start][da_const["obs_loc"]],zorder=3,label='observations')
        ax[c,1].scatter(m_const["x_grid"][da_const["obs_loc"]],states[j]['obs'][c+t_start][da_const["obs_loc"]],zorder=3,label='observations')
        ax[c,0].set_ylabel('assim step '+str(c+t_start) + '\n h [m]')
        
    
    ax[0,0].set_title('background ensemble and observations')
    ax[0,1].set_title('analysis ensemble after update')
    ax[0,1].set_xlim([m_const["x_grid"][0],m_const["x_grid"][-1]])
    plt.subplots_adjust(wspace=0.01,hspace=0.01)
#     ax[0].set_xticklabels([])
    ax[na-1,1].set_xlabel('x [m]')
    ax[na-1,0].set_xlabel('x [m]')
    plt.legend()
    return fig,ax


def quad_plotter(quad_state,m_const,da_const):
    """
    Plots the initial background and blind forecast, as well as the analysis and forecast with observations 
 
    Input: 
    j : experiment number
    t_start and t_end: time frame to plot

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')


    alpha = np.sqrt(1/da_const['nens'])
    
    fig, ax = plt.subplots(2,2,figsize=(10,5),sharex='all',sharey='all')
  
    
    
    for i in range(da_const["nens"]):
        ax[0,0].plot(m_const['x_grid'],quad_state['bg'][:,i],'r',alpha =alpha,zorder=1)
        ax[0,1].plot(m_const['x_grid'],quad_state['bf'][:,i],'b',alpha =alpha,zorder=1)
        ax[1,0].plot(m_const['x_grid'],quad_state['an'][:,i],'magenta',alpha =alpha,zorder=1)
        ax[1,1].plot(m_const['x_grid'],quad_state['fc'][:,i],'c',alpha =alpha,zorder=1)
    

    ax[0,0].plot(m_const["x_grid"],quad_state['tr_bg'],'k',zorder=10,label='truth')
    ax[1,0].plot(m_const["x_grid"],quad_state['tr_bg'],'k',zorder=10,label='truth')
    #ax[0,1].plot(m_const["x_grid"],quad_state['tr_fc'],'k',zorder=10,label='truth')
    #ax[1,1].plot(m_const["x_grid"],quad_state['tr_fc'],'k',zorder=10,label='truth')
    
    ax[0,0].plot(m_const['x_grid'],np.mean(quad_state['bg'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,0].plot(m_const['x_grid'],np.mean(quad_state['an'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[0,1].plot(m_const['x_grid'],np.mean(quad_state['bf'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,1].plot(m_const['x_grid'],np.mean(quad_state['fc'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')

    ax[0,0].scatter(m_const["x_grid"][da_const["obs_loc"]],quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
    ax[1,0].scatter(m_const["x_grid"][da_const["obs_loc"]],quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
        
    
    ax[0,0].set_title('background with obs')
    ax[0,1].set_title('free forecast')
    ax[1,0].set_title('analysis with obs')
    ax[1,1].set_title('forecast')
    ax[0,1].set_xlim([m_const["x_grid"][0],m_const["x_grid"][-1]])
    plt.subplots_adjust(wspace=0.03,hspace=0.30)
    ax[1,1].set_xlabel('x [m]')
    ax[1,0].set_xlabel('x [m]')
    ax[0,0].set_ylabel('h [m]')
    ax[1,0].set_ylabel('h [m]')
    ax[1,0].legend()
    return fig,ax

def quad_plotter_v2(quad_state,m_const,da_const):
    """
    Plots the initial background and blind forecast, as well as the analysis and forecast with observations 
 
    Input: 
    j : experiment number
    t_start and t_end: time frame to plot

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')


    alpha = np.sqrt(1/da_const['nens'])+0.1
    
    fig, ax = plt.subplots(2,2,figsize=(7.5,4),sharex='all',sharey='all')
  
    
    
    for i in range(da_const["nens"]):
        ax[0,0].plot(m_const['x_grid']/1000.,quad_state['bg'][:,i],'r',alpha =alpha,zorder=1)
        ax[0,1].plot(m_const['x_grid']/1000.,quad_state['bf'][:,i],'b',alpha =alpha,zorder=1)
        ax[1,0].plot(m_const['x_grid']/1000.,quad_state['an'][:,i],'magenta',alpha =alpha,zorder=1)
        ax[1,1].plot(m_const['x_grid']/1000.,quad_state['fc'][:,i],'c',alpha =alpha,zorder=1)
    

    ax[0,0].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    ax[1,0].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    #ax[0,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    #ax[1,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    
    ax[0,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bg'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['an'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[0,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['bf'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['fc'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')

    ax[0,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
    ax[1,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
    
    
    ax[0,0].set_title('background with observations')
    ax[0,1].set_title('free-forecast')
    ax[1,0].set_title('analysis with observations')
    ax[1,1].set_title('forecast')
    ax[0,1].set_xlim([m_const["x_grid"][0],m_const["x_grid"][-1]/1000])
    plt.subplots_adjust(wspace=0.03,hspace=0.20)
    ax[1,1].set_xlabel('x [km]')
    ax[1,0].set_xlabel('x [km]')
    ax[0,0].set_ylabel('h [m]')
    ax[1,0].set_ylabel('h [m]')
    ax[1,0].legend()
    
    # now to add in shading for response domain
    ylimits = ax[0,0].get_ylim()
    ax[0,1].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].set_ylim(ylimits)
    
    return fig,ax

def quad_plotter_paper(quad_state,m_const,da_const):
    """
    Plots the initial background and blind forecast, as well as the analysis and forecast with observations.

    Swapped free forecast and analysis figure locations, also changed h to phi

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')


    alpha = np.sqrt(1/da_const['nens'])+0.1
    
    fig, ax = plt.subplots(2,2,figsize=(7.5,4),sharex='all',sharey='all')
  
    
    
    for i in range(da_const["nens"]):
        ax[0,0].plot(m_const['x_grid']/1000.,quad_state['bg'][:,i],'r',alpha =alpha,zorder=1)
        ax[1,0].plot(m_const['x_grid']/1000.,quad_state['bf'][:,i],'b',alpha =alpha,zorder=1)
        ax[0,1].plot(m_const['x_grid']/1000.,quad_state['an'][:,i],'magenta',alpha =alpha,zorder=1)
        ax[1,1].plot(m_const['x_grid']/1000.,quad_state['fc'][:,i],'c',alpha =alpha,zorder=1)
    

    ax[0,0].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    ax[0,1].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    #ax[0,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    #ax[1,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    
    ax[0,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bg'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[0,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['an'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bf'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['fc'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')

    if da_const['n_obs_h']:
        ax[0,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
        ax[0,1].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
    
    
    ax[0,0].set_title('background')
    ax[1,0].set_title('free-forecast')
    ax[0,1].set_title('analysis')
    ax[1,1].set_title('forecast')
    ax[1,0].set_xlim([m_const["x_grid"][0],m_const["x_grid"][-1]/1000])
    plt.subplots_adjust(wspace=0.03,hspace=0.20)
    ax[1,1].set_xlabel('x [km]')
    ax[1,0].set_xlabel('x [km]')
    ax[0,0].set_ylabel(r'$\phi$')
    ax[1,0].set_ylabel(r'$\phi$')
    ax[0,1].legend()
    
    # now to add in shading for response domain
    ylimits = ax[0,0].get_ylim()
    ax[1,0].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].set_ylim(ylimits)
    
    return fig,ax

def B_plotter(states,j=0,ncyc=0,matrix='bg'):
    """
    Plots the covariance plotter of either the forecast/background ensemble ('bg'), analysis 'an', or the free/blind forecast 'bf'

    """
    
    
    fig, ax = plt.subplots(1,1,figsize=(5,5))

    ax.set_ylabel(r'$[\phi_1,\phi_2,\phi_3,...]$')
    ax.set_xlabel(r'$[\phi_1,\phi_2,\phi_3,...]$')

    #B = np.cov(ensemble_diff.transpose())
    B = np.cov(states[j][matrix][ncyc],ddof=1)
    limit = np.max(np.abs(B))/4.*3.
    pm = ax.pcolormesh(B,vmin=-limit,vmax=limit,cmap='RdBu_r')
    plt.colorbar(pm,shrink = 0.8)
    ax.set_aspect('equal')
    
    return fig,ax

def plot_scores_spread(rmse, spread):
    """
    Plots the evolution of the RMSE and spread over time and space.
    Could be expanded to also plot the blind forecast if available

    Todo:
    plot next to each other with a shared y axis
    """
    sns.set_style('whitegrid')
    figs = {}
    for dim in ["space", "time"]:
        figs[dim], ax = plt.subplots(1, sharex='col', figsize=(10, 4))
         
        #for i, var in enumerate(['RMSE over ' + dim + ' h', 'RMSE over ' + dim + ' u']):
        ax.set_title('RMSE over ' + dim, fontsize=24)
        ax.tick_params(labelsize=18)
        if 'bf' in rmse['time'].keys():
            ax.plot(rmse[dim]['bf'], 'b', label="bf rmse="+str(round(np.nanmean(rmse[dim]['bf']),3)))
            ax.plot(spread[dim]['bf'], 'b', label="bf spread="+str(round(np.nanmean(spread[dim]['bf']),3)), ls='--')
        ax.plot(rmse[dim]['bg']  , 'r'      , label="bg rmse="  +str(round(np.nanmean(rmse[dim]['bg']),3)),lw=3)
        ax.plot(spread[dim]['bg'], 'r'      , label="bg spread="+str(round(np.nanmean(spread[dim]['bg']),3)), ls='--',lw=3)
        ax.plot(rmse[dim]['an']  , 'magenta', label="an rmse="  +str(round(np.mean(rmse[dim]['an']),3)))
        ax.plot(spread[dim]['an'], 'magenta', label="an spread="+str(round(np.mean(spread[dim]['an']),3)), ls='--')
        

        ax.set_xlim(left=0) 
        ax.legend()
        if dim == "space":
            ax.set_xlabel("space in grid points", fontsize=18)
        if dim == "time":
            ax.set_xlabel("time in assimilation cycles", fontsize=18)
    return figs


def plot_response_function_pdf_split(states,j=0,t_start=0,t_end=0,left_var='bg',right_var='an'):
    """
    Makes a split Seaborn violin plot of the respone function values. By default plots the analysis and forecast, but can also plot the free/blind forecast
    Is not super efficient because it first transforms the response values into a panda dataframe to get the seaborn plot to work. 
    """
    import seaborn as sns
    import pandas as pd
    sns.set()
    fig,ax = plt.subplots(1,1,figsize=(6,4))

    ncyc,nens = np.array(states[j]['response']['bg']).shape
    if t_end==0: t_end=ncyc
    
    #Got to figure out how to make sure the times line up, since the time integers of an, bg, and bf are all 1 step delayed
    dict_label   ={'an':'analysis','bg':'forecast-t1','bf':'forecast-t2'}
    dict_color   ={'an':'magenta','bg':'r','bf':'b'}
    #t_diff = t_adjust[right_var]-t_adjust[left_var]
    
    nt = t_end-t_start
    
    
    plot_data = {
        'response' : np.hstack([np.array(states[j]['response'][left_var ][t_start:t_end]).ravel(),
                                np.array(states[j]['response'][right_var][t_start:t_end]).ravel()]),
        'cyc'      : np.hstack([np.repeat(np.arange(nt),nens),np.repeat(np.arange(nt),nens)])+t_start,
        'type'     : [dict_label[left_var]]*nt*nens+[dict_label[right_var]]*nt*nens}



    sns.violinplot(data=plot_data,y='response',x='cyc',hue='type', inner=None, orient="v",split=True,palette={dict_label[left_var]:dict_color[left_var],dict_label[right_var]:dict_color[right_var]},cut=0,bw=0.25)
    
    
    plt.plot(states[j]['response']['truth'][t_start:t_end],'k--',marker='o',label='truth')
    
    plt.legend(loc='lower center')
    ax.set_xlabel('assimilation step')
    ax.set_ylabel('PDF of response function J')
    
    #Moves the left and right pdf a bit appart so you can see where they end. Thank you stackoverflow 
    inner=None
    width = 0.75
    delta = 0.05
    final_width = width - delta
    offset_violinplot_halves(ax, delta, final_width, inner, 'vertical')
    return fig,ax




def offset_violinplot_halves(ax, delta, width, inner, direction):
    """
    From Stackoverflow!

    This function offsets the halves of a violinplot to compare tails
    or to plot something else in between them. This is specifically designed
    for violinplots by Seaborn that use the option `split=True`.

    For lines, this works on the assumption that Seaborn plots everything with
     integers as the center.

    Args:
     <ax>    The axis that contains the violinplots.
     <delta> The amount of space to put between the two halves of the violinplot
     <width> The total width of the violinplot, as passed to sns.violinplot()
     <inner> The type of inner in the seaborn
     <direction> Orientation of violinplot. 'hotizontal' or 'vertical'.

    Returns:
     - NA, modifies the <ax> directly
    """
    import matplotlib.collections
    # offset stuff
    if inner == 'sticks':
        lines = ax.get_lines()
        for line in lines:
            if direction == 'horizontal':
                data = line.get_ydata()
                print(data)
                if int(data[0] + 1)/int(data[1] + 1) < 1:
                    # type is top, move neg, direction backwards for horizontal
                    data -= delta
                else:
                    # type is bottom, move pos, direction backward for hori
                    data += delta
                line.set_ydata(data)
            elif direction == 'vertical':
                data = line.get_xdata()
                print(data)
                if int(data[0] + 1)/int(data[1] + 1) < 1:
                    # type is left, move neg
                    data -= delta
                else:
                    # type is left, move pos
                    data += delta
                line.set_xdata(data)


    for ii, item in enumerate(ax.collections):
        # axis contains PolyCollections and PathCollections
        if isinstance(item, matplotlib.collections.PolyCollection):
            # get path
            path, = item.get_paths()
            vertices = path.vertices
            half_type = _wedge_dir(vertices, direction)
            # shift x-coordinates of path
            if half_type in ['top','bottom']:
               if inner in ["sticks", None]:
                    if half_type == 'top': # -> up
                        vertices[:,1] -= delta
                    elif half_type == 'bottom': # -> down
                        vertices[:,1] += delta
            elif half_type in ['left', 'right']:
                if inner in ["sticks", None]:
                    if half_type == 'left': # -> left
                        vertices[:,0] -= delta
                    elif half_type == 'right': # -> down
                        vertices[:,0] += delta

def _wedge_dir(vertices, direction):
    """
    Args:
      <vertices>  The vertices from matplotlib.collections.PolyCollection
      <direction> Direction must be 'horizontal' or 'vertical' according to how
                   your plot is laid out.
    Returns:
      - a string in ['top', 'bottom', 'left', 'right'] that determines where the
         half of the violinplot is relative to the center.
    """
    if direction == 'horizontal':
        result = (direction, len(set(vertices[1:5,1])) == 1)
    elif direction == 'vertical':
        result = (direction, len(set(vertices[-3:-1,0])) == 1)
    outcome_key = {('horizontal', True): 'bottom',
                   ('horizontal', False): 'top',
                   ('vertical', True): 'left',
                   ('vertical', False): 'right'}
    # if the first couple x/y values after the start are the same, it
    #  is the input direction. If not, it is the opposite
    return outcome_key[result]


#Little subplot labeling script found online from https://gist.github.com/tacaswell/9643166
#Put here for no good reason
#Warning, if you define your colorbar using an additional axes it will give that a label as well, which is pretty funny but also very annoying. 

import string
from itertools import cycle
from six.moves import zip
def label_axes_abcd(fig, labels=None, loc=None, **kwargs):
    """
    Walks through axes and labels each.
    kwargs are collected and passed to `annotate`
    Parameters
    ----------
    fig : Figure
         Figure object to work on
    labels : iterable or None
        iterable of strings to use to label the axes.
        If None, lower case letters are used.
    
    loc : len=2 tuple of floats
        Where to put the label in axes-fraction units
    """
    if labels is None:
        labels = string.ascii_lowercase
    
    # re-use labels rather than stop labeling
    labels = cycle(labels)
    if loc is None:
        loc = (.9, .9)
    for ax, lab in zip(fig.axes, labels):
        ax.annotate(lab, xy=loc,
                    xycoords='axes fraction',
                    **kwargs)
###########################################################################################
# 2022 baby, now with sat data
###########################################################################################


def ensemble_plotter_22(states,m_const,da_const,j=0,t_start=0,t_end=0,h_c=None):
    """
    Plots the full forecast/background ensemble with observations, as well as the resulting analysis.
    Plots all timesteps from t_start till t_end. 
    Initial starting conditions have no background state
    
    
 
    Input: 
    j : experiment number
    t_start and t_end: time frame to plot

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')
    nc = da_const['ncyc']
    if t_end ==0: t_end = nc

    na = t_end-t_start

    alpha = np.sqrt(1/da_const['nens'])
    
    fig, ax = plt.subplots(na,2,figsize=(7.5,na*1.5),sharex='all',sharey='all')
  
    #left bg, right analysis
    
    
    #for c in range(nc):
    for c in range(na):
        for i in range(da_const["nens"]):
            ax[c,0].plot(m_const['x_grid']/1000.,states[j]['bg'][c+t_start][:,i],'r',alpha =alpha,zorder=1)
            ax[c,1].plot(m_const['x_grid']/1000.,states[j]['an'][c+t_start][:,i],'magenta',alpha =alpha,zorder=1)
        ax[c,0].plot(m_const['x_grid']/1000.,np.mean(states[j]['bg'][c+t_start][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
        ax[c,0].plot(m_const["x_grid"]/1000.,states[j]['truth'][c+t_start],'k',zorder=10,label='truth')
        
        ax[c,1].plot(m_const['x_grid']/1000.,np.mean(states[j]['an'][c+t_start][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
        ax[c,1].plot(m_const["x_grid"]/1000.,states[j]['truth'][c+t_start],'k',zorder=10,label='truth')
        
        # if da_const['n_obs_h']>0:
        if da_const['n_obs_h']:
            ax[c,0].scatter( m_const["x_grid"][da_const["obs_loc"]]/1000.,states[j]['obs'][c+t_start][da_const["obs_loc"]]   ,zorder=3,s=10,label='obs',color='k')
            ax[c,1].scatter( m_const["x_grid"][da_const["obs_loc"]]/1000.,states[j]['obs'][c+t_start][da_const["obs_loc"]]   ,zorder=3,s=10,label='obs',color='k')
            ax[c,1].errorbar(m_const["x_grid"][da_const["obs_loc"]]/1000.,states[j]['obs'][c+t_start][da_const["obs_loc"]]   ,yerr=da_const['used_std_obs'] ,zorder=3,color='k',capsize=3,capthick=2,ls='none')
            ax[c,0].errorbar(m_const["x_grid"][da_const["obs_loc"]]/1000.,states[j]['obs'][c+t_start][da_const["obs_loc"]]   ,yerr=da_const['used_std_obs'] ,zorder=3,color='k',capsize=3,capthick=2,ls='none')
        if h_c is not None:
            ax[c,0].hlines(h_c,m_const['x_grid'][0]/1000.,m_const['x_grid'][-1]/1000.,color='k',ls=':',label='sat threshold')
            ax[c,1].hlines(h_c,m_const['x_grid'][0]/1000.,m_const['x_grid'][-1]/1000.,color='k',ls=':',label='sat threshold')

        # ax[c,0].set_ylabel('assim step: ' + str(c+t_start)+ '\n' +r'$\phi$') 
        ax[c,0].set_ylabel('step: ' + str(c+t_start)+ '\n' +r'$\phi$') 
    
    ax[0,0].set_title('background ensemble \n and observations')
    ax[0,1].set_title('analysis ensemble \n after assimilation')
    ax[0,1].set_xlim([m_const["x_grid"][0]/1000.,m_const["x_grid"][-1]/1000.])
    plt.subplots_adjust(wspace=0.01,hspace=0.01)
#     ax[0].set_xticklabels([])
    ax[na-1,1].set_xlabel('x [km]')
    ax[na-1,0].set_xlabel('x [km]')
    plt.legend()
    return fig,ax




def quad_plotter_22(quad_state,m_const,da_const):
    """
    Plots the initial background and blind forecast, as well as the analysis and forecast with observations.

    Only difference to previous versions is that it can deal with no obs being present
 

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')


    alpha = np.sqrt(1/da_const['nens'])+0.1
    
    fig, ax = plt.subplots(2,2,figsize=(7.5,4),sharex='all',sharey='all')
  
    
    
    for i in range(da_const["nens"]):
        ax[0,0].plot(m_const['x_grid']/1000.,quad_state['bg'][:,i],'r',alpha =alpha,zorder=1)
        ax[0,1].plot(m_const['x_grid']/1000.,quad_state['bf'][:,i],'b',alpha =alpha,zorder=1)
        ax[1,0].plot(m_const['x_grid']/1000.,quad_state['an'][:,i],'magenta',alpha =alpha,zorder=1)
        ax[1,1].plot(m_const['x_grid']/1000.,quad_state['fc'][:,i],'c',alpha =alpha,zorder=1)
    

    ax[0,0].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    ax[1,0].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    #ax[0,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    #ax[1,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    
    ax[0,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bg'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['an'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[0,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['bf'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['fc'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')

    if da_const['n_obs_h']:
        ax[0,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
        ax[1,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
    
    
    ax[0,0].set_title('background')
    ax[0,1].set_title('free-forecast')
    ax[1,0].set_title('analysis')
    ax[1,1].set_title('forecast')
    ax[0,1].set_xlim([m_const["x_grid"][0],m_const["x_grid"][-1]/1000])
    plt.subplots_adjust(wspace=0.03,hspace=0.20)
    ax[1,1].set_xlabel('x [km]')
    ax[1,0].set_xlabel('x [km]')
    ax[0,0].set_ylabel('h [m]')
    ax[1,0].set_ylabel('h [m]')
    ax[1,0].legend()
    
    # now to add in shading for response domain
    ylimits = ax[0,0].get_ylim()
    ax[0,1].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].set_ylim(ylimits)
    
    return fig,ax


def quad_plotter_25(quad_state,m_const,da_const):
    """
    Plots the initial background and blind forecast, as well as the analysis and forecast with observations.
    
    Difference to earlier _22 is the arrangement of the subplots, and the addition of uncertainity bars for the observations. 
 

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')


    alpha = np.sqrt(1/da_const['nens'])+0.1
    
    fig, ax = plt.subplots(2,2,figsize=(7.5,4),sharex='all',sharey='all')
  
    
    
    for i in range(da_const["nens"]):
        ax[0,0].plot(m_const['x_grid']/1000.,quad_state['bg'][:,i],'r',alpha =alpha,zorder=1)
        ax[1,0].plot(m_const['x_grid']/1000.,quad_state['bf'][:,i],'b',alpha =alpha,zorder=1)
        ax[0,1].plot(m_const['x_grid']/1000.,quad_state['an'][:,i],'magenta',alpha =alpha,zorder=1)
        ax[1,1].plot(m_const['x_grid']/1000.,quad_state['fc'][:,i],'c',alpha =alpha,zorder=1)
    

    ax[0,0].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    ax[0,1].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    #ax[0,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    #ax[1,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    
    ax[0,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bg'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[0,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['an'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bf'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['fc'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')

    if da_const['n_obs_h']:
        ax[0,0].scatter( m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,zorder=3,s=10,label='obs',color='k')
        ax[0,1].scatter( m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,zorder=3,s=10,label='obs',color='k')
        ax[0,1].errorbar(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,yerr=da_const['used_std_obs'] ,zorder=3,color='k',capsize=3,capthick=2,ls='none')
        ax[0,0].errorbar(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,yerr=da_const['used_std_obs'] ,zorder=3,color='k',capsize=3,capthick=2,ls='none')
        # ax[0,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
        # ax[1,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
    
    
    ax[0,0].set_title('background')
    ax[1,0].set_title('free-forecast')
    ax[0,1].set_title('analysis')
    ax[1,1].set_title('forecast')
    ax[0,1].set_xlim([m_const["x_grid"][0],m_const["x_grid"][-1]/1000])
    plt.subplots_adjust(wspace=0.03,hspace=0.20)
    ax[1,1].set_xlabel('x [km]')
    ax[1,0].set_xlabel('x [km]')
    ax[0,0].set_ylabel(r'$\phi$')
    ax[1,0].set_ylabel(r'$\phi$')
    ax[1,0].legend()
    
    # now to add in shading for response domain
    ylimits = ax[0,0].get_ylim()
    ax[1,0].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].fill_between([10,20], ylimits[0], y2=ylimits[1],color='grey',alpha=0.2,zorder=-1)
    ax[1,1].set_ylim(ylimits)
    
    return fig,ax

def quad_plotter_hh(quad_state,m_const,da_const):
    """
    Plots the initial background and blind forecast, as well as the analysis and forecast with observations.
    
    Intended for Herbert Hartl, but only difference to _25 is that the grey shading used to show the middle of the domain is not used. 
 

    Returns: 
    figure and axes
    """
    sns.set()
    sns.set_style('whitegrid')


    alpha = np.sqrt(1/da_const['nens'])+0.1
    
    fig, ax = plt.subplots(2,2,figsize=(7.5,4),sharex='all',sharey='all')
  
    
    
    for i in range(da_const["nens"]):
        ax[0,0].plot(m_const['x_grid']/1000.,quad_state['bg'][:,i],'r',alpha =alpha,zorder=1)
        ax[1,0].plot(m_const['x_grid']/1000.,quad_state['bf'][:,i],'b',alpha =alpha,zorder=1)
        ax[0,1].plot(m_const['x_grid']/1000.,quad_state['an'][:,i],'magenta',alpha =alpha,zorder=1)
        ax[1,1].plot(m_const['x_grid']/1000.,quad_state['fc'][:,i],'c',alpha =alpha,zorder=1)
    

    ax[0,0].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    ax[0,1].plot(m_const["x_grid"]/1000.,quad_state['tr_bg'],'k',zorder=10,label='truth')
    #ax[0,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    #ax[1,1].plot(m_const["x_grid"],quad_stat/1000.e['tr_fc'],'k',zorder=10,label='truth')
    
    ax[0,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bg'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[0,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['an'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,0].plot(m_const['x_grid']/1000.,np.mean(quad_state['bf'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')
    ax[1,1].plot(m_const['x_grid']/1000.,np.mean(quad_state['fc'][:,:],axis=1),'k--',alpha =1,zorder=2,label='ens mean')

    if da_const['n_obs_h']:
        ax[0,0].scatter( m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,zorder=3,s=10,label='obs',color='k')
        ax[0,1].scatter( m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,zorder=3,s=10,label='obs',color='k')
        ax[0,1].errorbar(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,yerr=da_const['used_std_obs'] ,zorder=3,color='k',capsize=3,capthick=2,ls='none')
        ax[0,0].errorbar(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]]   ,yerr=da_const['used_std_obs'] ,zorder=3,color='k',capsize=3,capthick=2,ls='none')
        # ax[0,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
        # ax[1,0].scatter(m_const["x_grid"][da_const["obs_loc"]]/1000.,quad_state['obs'][da_const["obs_loc"]],zorder=3,label='observations',color='k')
    
    
    ax[0,0].set_title('background')
    ax[1,0].set_title('free-forecast')
    ax[0,1].set_title('analysis')
    ax[1,1].set_title('forecast')
    ax[0,1].set_xlim([m_const["x_grid"][0],m_const["x_grid"][-1]/1000])
    plt.subplots_adjust(wspace=0.03,hspace=0.20)
    ax[1,1].set_xlabel('x [km]')
    ax[1,0].set_xlabel('x [km]')
    ax[0,0].set_ylabel(r'$\phi$')
    ax[1,0].set_ylabel(r'$\phi$')
    ax[1,0].legend()
    

    
    return fig,ax

def plot_ensemble_sat_analysis(bg,an,obs,obs_sat,truth,sat_operator,m_const,da_const,h_c=None):
    fig ,ax = plt.subplots(2,2,figsize=(10,6),sharey='col',sharex='all')
    """
    Plots the background and analysis ensemble, as well as the satetlitte obs equivalents. Includes observations as well.
    
    Is still quite rough around the edges, and will hopefully be improced upon
    """

    sat_an    = sat_operator(an)
    sat_bg    = sat_operator(bg)
    sat_tr_bg = sat_operator(truth)
    n_obs_h  =len(da_const["obs_loc"])
    n_obs_sat =len(da_const["obs_loc_sat"])
     
    dY_b, y_ol_b = state_to_observation_space(bg,m_const,da_const,sat_operator)
    y_b = dY_b.T + y_ol_b
    y_b = y_b.T
    
    
    # for plotting the pixels
    window = m_const['x_grid'][da_const["obs_loc_sat"][1]]-m_const['x_grid'][da_const["obs_loc_sat"][0]]
    xpixmin = m_const['x_grid'][da_const["obs_loc_sat"]]-window/2 
    xpixmax = m_const['x_grid'][da_const["obs_loc_sat"]]+window/2 
    
    for i in range(bg.shape[1]):
        #ax[0].plot(m_const['x_grid'],x_a[:,i],'r',alpha=0.2)
        ax[1,0].plot(m_const['x_grid']/1000,an [:,i],'magenta',alpha=0.2)
        ax[0,0].plot(m_const['x_grid']/1000,bg [:,i],'r',alpha=0.2)
        
        ax[1,1].hlines(
                     sat_an [da_const["obs_loc_sat"],i],
                     xpixmin/1000,xpixmax/1000,
                     color='magenta',alpha=0.2,lw=3)
        ax[0,1].hlines(
                     sat_bg [da_const["obs_loc_sat"],i],
                     xpixmin/1000,xpixmax/1000,
                     color='r',alpha=0.2,lw=3)
    ax[1,0].plot(m_const['x_grid']/1000+1000,an [:,-1],'magenta',alpha=1,label='background ensemble')
    ax[0,0].plot(m_const['x_grid']/1000+1000,bg [:,-1],'r',alpha=1,label='analysis ensemble')
    
    #ax[0].plot(m_const['x_grid'],x_ol_a,'r')
    ax[1,0].plot(m_const['x_grid']/1000,truth,'k',linewidth=2,label='truth')
    ax[0,0].plot(m_const['x_grid']/1000,truth,'k',linewidth=2)


    if n_obs_h>0:
        ax[1,0].scatter(m_const['x_grid'][da_const['obs_loc']]/1000,obs[da_const['obs_loc']],
                        c='k',label='h point observation')
        ax[0,0].scatter(m_const['x_grid'][da_const['obs_loc']]/1000,obs[da_const['obs_loc']],c='k')#,label='h point obs')
    if n_obs_sat>0:
        ax[1,1].scatter(m_const['x_grid'][da_const['obs_loc_sat']]/1000,obs_sat[da_const['obs_loc_sat']],
                        marker='x',s=50,c='k',label='reflectance observation')
        ax[0,1].scatter(m_const['x_grid'][da_const['obs_loc_sat']]/1000,obs_sat[da_const['obs_loc_sat']],marker='x',s=50,c='k')


    ax[0,0].plot(m_const['x_grid']/1000,np.mean(bg,axis=1),'k--',lw=2,label='ensemble mean')
    ax[1,0].plot(m_const['x_grid']/1000,np.mean(an,axis=1),'k--',lw=2)#,label='ens mean')
    
    if h_c is not None:  
        ax[0,0].hlines(h_c,m_const['x_grid'][0]/1000,m_const['x_grid'][-1]/1000,color='k',ls=':')
        ax[1,0].hlines(h_c,m_const['x_grid'][0]/1000,m_const['x_grid'][-1]/1000,color='k',ls=':',label=r'cloud threshold $h_c$')
    
    label_axes_abcd(fig)

    ax1 = ax[0,1].twinx()
    ax1.set_yticks([0.3,0.7])
    ax1.set_yticklabels(['clear \n sky','cloud'])
    ax2 = ax[1,1].twinx()
    ax2.set_yticks([0.3,0.7])
    ax2.set_yticklabels(['clear \n sky','cloud'])
    ax1.set_ylim(-0.05,1.05)
    ax2.set_ylim(-0.05,1.05)
    ax[1,1].set_ylim(-0.05,1.05)
    ax[0,0].set_yticks([0,0.5,1])
    ax[0,1].set_yticks([0,0.3,0.7,1.0])
    ax[0,0].set_yticklabels(['0',r'$h_c$','1'])
    
    ax[1,0].set_xlabel('x [km]')
    ax[1,1].set_xlabel('x [km]')
    ax[1,1].set_xlim(m_const['x_grid'][0]/1000,30)#m_const['x_grid'][-1]/1000)
    ax[0,0].set_ylabel('background \n h [m]')
    ax[1,0].set_ylabel('analysis \n h[m]')
    ax[0,1].set_ylabel('reflectance')
    ax[1,1].set_ylabel('reflectance')
    plt.subplots_adjust(wspace=0.2,hspace=0.1)
    
    ax[0,1].step(m_const['x_grid'][da_const['obs_loc_sat']]/1000,np.mean(sat_bg[da_const['obs_loc_sat']],axis=1),'k--',lw=2,where='mid')#,marker='s',markersize=4,label='ens mean')
    ax[1,1].step(m_const['x_grid'][da_const['obs_loc_sat']]/1000,np.mean(sat_an[da_const['obs_loc_sat']],axis=1),'k--',lw=2,where='mid')#,marker='s',markersize=4,label='ens mean')
    
    lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
    lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
    fig.legend(lines, labels, loc='upper center',ncol=4)
    return fig, ax



def plot_ensemble_sat_analysis_paper(bg,an,obs,obs_sat,truth,sat_operator,m_const,da_const,h_c=None):
    fig ,ax = plt.subplots(2,2,figsize=(7.5,5),sharey='row',sharex='all')
    """
    Plots the background and analysis ensemble of the state, as well as the satetlitte obs equivalents. Includes observations as well.
    """

    sat_an    = sat_operator(an)
    sat_bg    = sat_operator(bg)
    sat_tr_bg = sat_operator(truth)
    n_obs_h  =len(da_const["obs_loc"])
    n_obs_sat =len(da_const["obs_loc_sat"])
     
    dY_b, y_ol_b = state_to_observation_space(bg,m_const,da_const,sat_operator)
    y_b = dY_b.T + y_ol_b
    y_b = y_b.T
    
    
    # for plotting the pixels
    window = m_const['x_grid'][da_const["obs_loc_sat"][1]]-m_const['x_grid'][da_const["obs_loc_sat"][0]]
    xpixmin = m_const['x_grid'][da_const["obs_loc_sat"]]-window/2 
    xpixmax = m_const['x_grid'][da_const["obs_loc_sat"]]+window/2 
    
    for i in range(bg.shape[1]):
        #ax[0].plot(m_const['x_grid'],x_a[:,i],'r',alpha=0.2)
        ax[0,1].plot(m_const['x_grid']/1000,an [:,i],'magenta',alpha=0.2)
        ax[0,0].plot(m_const['x_grid']/1000,bg [:,i],'r',alpha=0.2)
        
        # plotting the pixels
         
        #ax[1,1].plot(m_const['x_grid'],sat_an [:,i],'magenta',alpha=0.2)
        #ax[0,1].plot(m_const['x_grid'],sat_bg [:,i],'r',alpha=0.2)
        ax[1,1].hlines(
                     sat_an [da_const["obs_loc_sat"],i],
                     xpixmin/1000,xpixmax/1000,
                     color='magenta',alpha=0.2,lw=3)
        ax[1,0].hlines(
                     sat_bg [da_const["obs_loc_sat"],i],
                     xpixmin/1000,xpixmax/1000,
                     color='r',alpha=0.2,lw=3)
    ax[0,1].plot(m_const['x_grid']/1000+1000,an [:,-1],'magenta',alpha=1,label='analysis ensemble')
    ax[0,0].plot(m_const['x_grid']/1000+1000,bg [:,-1],'r',alpha=1,label='background ensemble')
    
    #ax[0].plot(m_const['x_grid'],x_ol_a,'r')
    ax[0,1].plot(m_const['x_grid']/1000,truth,'k',linewidth=2,label='truth')
    ax[0,0].plot(m_const['x_grid']/1000,truth,'k',linewidth=2)


    if n_obs_h>0:
        ax[0,1].scatter(m_const['x_grid'][da_const['obs_loc']]/1000,obs[da_const['obs_loc']],
                        c='k',label=r'$\phi$ point observation')
        ax[0,0].scatter(m_const['x_grid'][da_const['obs_loc']]/1000,obs[da_const['obs_loc']],c='k')#,label='h point obs')
    if n_obs_sat>0:
        ax[1,1].scatter(m_const['x_grid'][da_const['obs_loc_sat']]/1000,obs_sat[da_const['obs_loc_sat']],
                        marker='x',s=50,c='k',label='reflectance observation')
        ax[1,0].scatter(m_const['x_grid'][da_const['obs_loc_sat']]/1000,obs_sat[da_const['obs_loc_sat']],marker='x',s=50,c='k')

    ax[0,0].plot(m_const['x_grid']/1000,np.mean(bg,axis=1),'k--',lw=2,label='ensemble mean')
    ax[0,1].plot(m_const['x_grid']/1000,np.mean(an,axis=1),'k--',lw=2)#,label='ens mean')
    
    if h_c is not None:  
        ax[0,0].hlines(h_c,m_const['x_grid'][0]/1000,m_const['x_grid'][-1]/1000,color='k',ls=':')
        ax[0,1].hlines(h_c,m_const['x_grid'][0]/1000,m_const['x_grid'][-1]/1000,color='k',ls=':',label=r'cloud threshold $\phi_c$')
    
    label_axes_abcd(fig)

    ax1 = ax[0,1].twinx()
    ax1.set_yticks([0.,0.5,1])
    ax1.set_yticklabels(['0',r'$\phi_c$','1'])
    ax1.set_ylim(-0.3,1.3)
    ax[0,0].set_ylim(-0.3,1.3)
    ax2 = ax[1,1].twinx()
    ax2.set_yticks([0.3,0.7])
    ax2.set_yticklabels(['clear \n sky','cloud'])
    ax2.set_ylim(-0.05,1.05)
    ax[1,1].set_ylim(-0.05,1.05)
    ax[0,0].set_yticks([0,0.5,1])
    ax[1,0].set_yticks([0,0.3,0.7,1.0])
    ax[0,0].set_yticklabels(['0',r'$\phi_c$','1'])
    
    
    ax[1,0].set_xlabel('x [km]')
    ax[1,1].set_xlabel('x [km]')
    ax[0,1].set_xlim(m_const['x_grid'][0]/1000,30)#m_const['x_grid'][-1]/1000)
    ax[0,0].set_title('background')
    ax[0,1].set_title('analysis')
    ax[1,0].set_ylabel('reflectance')
    ax[0,0].set_ylabel(r'$\phi$')
    plt.subplots_adjust(wspace=0.05,hspace=0.05)
    
    ax[1,0].step(m_const['x_grid'][da_const['obs_loc_sat']]/1000,np.mean(sat_bg[da_const['obs_loc_sat']],axis=1),'k--',lw=2,where='mid')#,marker='s',markersize=4,label='ens mean')
    ax[1,1].step(m_const['x_grid'][da_const['obs_loc_sat']]/1000,np.mean(sat_an[da_const['obs_loc_sat']],axis=1),'k--',lw=2,where='mid')#,marker='s',markersize=4,label='ens mean')
    
    lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
    lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
    fig.legend(lines, labels, loc='upper center',bbox_to_anchor=(0.5,1.1),ncol=3)
    return fig, ax


def plot_ensemble_sat_analysis_abstract(bg,an,obs,obs_sat,truth,sat_operator,m_const,da_const,h_c=None):
    fig ,ax = plt.subplots(2,2,figsize=(5.5,4.5),sharey='row',sharex='all')
    """
    Modified for paper visual abstract
    Plots the background and analysis ensemble, as well as the satetlitte obs equivalents. Includes observations as well.
    """

    sat_an    = sat_operator(an)
    sat_bg    = sat_operator(bg)
    sat_tr_bg = sat_operator(truth)
    n_obs_h  =len(da_const["obs_loc"])
    n_obs_sat =len(da_const["obs_loc_sat"])
     
    dY_b, y_ol_b = state_to_observation_space(bg,m_const,da_const,sat_operator)
    y_b = dY_b.T + y_ol_b
    y_b = y_b.T
    
    
    # for plotting the pixels
    window = m_const['x_grid'][da_const["obs_loc_sat"][1]]-m_const['x_grid'][da_const["obs_loc_sat"][0]]
    xpixmin = m_const['x_grid'][da_const["obs_loc_sat"]]-window/2 
    xpixmax = m_const['x_grid'][da_const["obs_loc_sat"]]+window/2 
    
    for i in range(bg.shape[1]):
        #ax[0].plot(m_const['x_grid'],x_a[:,i],'r',alpha=0.2)
        ax[0,1].plot(m_const['x_grid']/1000,an [:,i],'magenta',alpha=0.2)
        ax[0,0].plot(m_const['x_grid']/1000,bg [:,i],'r',alpha=0.2)
        
        ax[1,1].hlines(
                     sat_an [da_const["obs_loc_sat"],i],
                     xpixmin/1000,xpixmax/1000,
                     color='magenta',alpha=0.2,lw=3)
        ax[1,0].hlines(
                     sat_bg [da_const["obs_loc_sat"],i],
                     xpixmin/1000,xpixmax/1000,
                     color='r',alpha=0.2,lw=3)
    ax[0,1].plot(m_const['x_grid']/1000+1000,an [:,-1],'magenta',alpha=1,label='analysis ensemble')
    ax[0,0].plot(m_const['x_grid']/1000+1000,bg [:,-1],'r',alpha=1,label='background ensemble')
    
    #ax[0].plot(m_const['x_grid'],x_ol_a,'r')
    ax[0,1].plot(m_const['x_grid']/1000,truth,'k',linewidth=2,label='truth')
    ax[0,0].plot(m_const['x_grid']/1000,truth,'k',linewidth=2)

    if n_obs_h>0:
        ax[0,1].scatter(m_const['x_grid'][da_const['obs_loc']]/1000,obs[da_const['obs_loc']],
                        c='k',label=r'$\phi$ point observation')
        ax[0,0].scatter(m_const['x_grid'][da_const['obs_loc']]/1000,obs[da_const['obs_loc']],c='k')#,label='h point obs')
    if n_obs_sat>0:
        ax[1,1].scatter(m_const['x_grid'][da_const['obs_loc_sat']]/1000,obs_sat[da_const['obs_loc_sat']],
                        marker='x',s=50,c='k',label='reflectance observation')
        ax[1,0].scatter(m_const['x_grid'][da_const['obs_loc_sat']]/1000,obs_sat[da_const['obs_loc_sat']],marker='x',s=50,c='k')


    ax[0,0].plot(m_const['x_grid']/1000,np.mean(bg,axis=1),'k--',lw=2,label='ensemble mean')
    ax[0,1].plot(m_const['x_grid']/1000,np.mean(an,axis=1),'k--',lw=2)#,label='ens mean')
    
    if h_c is not None:  
        ax[0,0].hlines(h_c,m_const['x_grid'][0]/1000,m_const['x_grid'][-1]/1000,color='k',ls=':')
        ax[0,1].hlines(h_c,m_const['x_grid'][0]/1000,m_const['x_grid'][-1]/1000,color='k',ls=':',label=r'cloud threshold $\phi_c$')
    

    ax1 = ax[0,1].twinx()
    ax1.set_yticks([0.,0.5,1])
    ax1.set_yticklabels(['0',r'$\phi_c$','1'])
    ax1.set_ylim(-0.3,1.3)
    ax[0,0].set_ylim(-0.3,1.3)
    ax2 = ax[1,1].twinx()
    ax2.set_yticks([0.3,0.7])
    ax2.set_yticklabels(['clear \n sky','cloud'])
    ax2.set_ylim(-0.05,1.05)
    ax[1,1].set_ylim(-0.05,1.05)
    ax[0,0].set_yticks([0,0.5,1])
    ax[1,0].set_yticks([0.3,0.7])
    ax[1,0].set_yticklabels([])
    ax[0,0].set_yticklabels([])
    ax[1,0].set_xticklabels([])
    ax[1,1].set_xticklabels([])
    
    
    ax[1,0].set_xlabel('x',labelpad=-8)
    ax[1,1].set_xlabel('x',labelpad=-8)
    ax[0,1].set_xlim(m_const['x_grid'][0]/1000,30)#m_const['x_grid'][-1]/1000)
    ax[0,0].set_title('background')
    ax[0,1].set_title('analysis')
    ax[0,0].set_ylabel(r'$\phi$',labelpad=-10)
    ax[1,0].set_ylabel('reflectance',labelpad=-8)
    plt.subplots_adjust(wspace=0.05,hspace=0.05)
    
    ax[1,0].step(m_const['x_grid'][da_const['obs_loc_sat']]/1000,np.mean(sat_bg[da_const['obs_loc_sat']],axis=1),'k--',lw=2,where='mid')#,marker='s',markersize=4,label='ens mean')
    ax[1,1].step(m_const['x_grid'][da_const['obs_loc_sat']]/1000,np.mean(sat_an[da_const['obs_loc_sat']],axis=1),'k--',lw=2,where='mid')#,marker='s',markersize=4,label='ens mean')
    
    lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
    lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
    return fig, ax

def plot_J_quad_paper(J_dict,quad,sens,dx,bw=0.3,dJ=True):
    """
    Plots the forecast metric distributions of the free forecast, forecast, and their linear approximations for the given sensitivity
    """
    
    fig = plt.figure(figsize=(4,3))
    nens = len(J_dict['bf'])
    dX_bg=(quad['bg'].T-np.mean(quad['bg'],axis=1)).T
    dX_an=(quad['an'].T-np.mean(quad['an'],axis=1)).T
    dX_an=dx
    dJ_ff=np.dot(sens,dX_bg)
    dJ_fc=np.dot(sens,dX_an)
    print('estimated and real var reductions:',np.var(dJ_fc,ddof=1)-np.var(dJ_ff,ddof=1 ),np.var(J_dict['fc'],ddof=1)-np.var(J_dict['bf'],ddof=1))
    print('variance of free forecast, estimated variance of ff, and the same for the forecast:',np.var(J_dict['bf'],ddof=1),np.var(dJ_ff,ddof=1),np.var(J_dict['fc'],ddof=1),np.var(dJ_fc,ddof=1 ))
            #'response' : np.hstack([J_dict['bf']-np.mean(J_dict['bf']),dJ_ff,J_dict['fc']-np.mean(J_dict['fc']),J_dict['es']-np.mean(J_dict['es'])]),
    if dJ:
        plot_data = {
            'response' : np.hstack([J_dict['bf']-np.mean(J_dict['bf']),dJ_ff,J_dict['fc']-np.mean(J_dict['fc']),dJ_fc]),
            'x_pos'    : np.hstack([np.zeros(nens)+0,np.zeros(nens)+1,np.zeros(nens)+2,np.zeros(nens)+3]),
            'type'     : ['blindforecast']*nens+['estimated']*nens+['forecast']*nens}
    else:
        plot_data = {
            'response' : np.hstack([J_dict['bf'],dJ_ff+np.mean(J_dict['bf']),J_dict['fc'],dJ_fc+np.mean(J_dict['fc'])]),
            'x_pos'    : np.hstack([np.zeros(nens)+0,np.zeros(nens)+1,np.zeros(nens)+2,np.zeros(nens)+3]),
            'type'     : ['blindforecast']*nens+['estimated']*nens+['forecast']*nens}

    my_pal = ["blue",  "peru","cyan","orange"  ]
        
    PROPS = {
    'boxprops':{'facecolor':'none', 'edgecolor':'black'},
    }
    #ax = sns.violinplot(data=plot_data, inner='quartile', orient="v",cut=0,bw=bw,y='response',x='x_pos',palette=my_pal)#sns.color_palette('cool',n_colors=3))#,x='type')#,y='response',x='cyc',hue='type',,split=True,palette={dict_label[left_var]:dict_color[left_var],dict_label[right_var]:dict_color[right_var]}
    ax = sns.stripplot(data=plot_data, y='response', x='x_pos', hue='x_pos', alpha=0.7, jitter=0.15, size=5, palette=my_pal, legend=False)#color='0.0')#
    # ax = sns.stripplot(data=plot_data, y='response',x='x_pos',alpha=0.7,jitter=0.15,size=5,palette=my_pal)#color='0.0')#
    #ax = sns.boxplot(data=plot_data, y='response',x='x_pos',showfliers=False,**PROPS)#,patch_artist=False)#color='0.0')#,palette=my_pal
    #plot errorbars
    plt.errorbar(np.arange(4),np.zeros(4),[np.std(J_dict['bf'],ddof=1),np.std(dJ_ff,ddof=1),np.std(J_dict['fc'],ddof=1),np.std(dJ_fc,ddof=1 )],fmt='.',capsize=15,lw=3,color='k') 
    
    #if dJ == False: ax.hlines(J_dict['tr_fc'],-0.5,2.5,'k',ls='--',label='truth'); plt.legend()
    #if dJ: ax.hlines(0,-0.5,3.5,'k',ls='--') 
    ax.set_xlim(-0.5,3.5)
    if dJ == False: ax.set_ylabel(r'$j$')
    if dJ: ax.set_ylabel(r'$\delta j$')
    ax.set_xticklabels(['free-\nforecast','estimated \n free-forecast','\n forecast','estimated \n forecast'])
    return fig, ax

def vr_scatter_v6(vr_tot1,vr_rea1,vr_tot2,vr_rea2,vr_tot3,vr_rea3,vr_tot4,vr_tot5,vr_tot6,alpha=0.3,alpha2=0.5,color1='blueviolet',color2=plt.cm.viridis(0.8),
                  label1='',label2='',label3='',llabel1='explicit sens',llabel2='implicit sens'):
    """
    Just a 2x3 scatterplot with shared axis and a linear regressions """
    
    fig, ax = plt.subplots(2,3,figsize=(8,5.5),sharex='all',sharey='all')
    
    color = color1
    vr_rea = vr_rea1
    vr_tot = vr_tot1
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[0,0].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[0,0].scatter(vr_rea,vr_tot,c=color,alpha=alpha,s=5,zorder=1,label=llabel1)
    ax[0,0].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[0,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[0,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[0,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[0,0].set_aspect('equal', 'box')
    ax[0,0].legend(loc='lower center')
    
            
    vr_rea = vr_rea2
    vr_tot = vr_tot2
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[0,1].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[0,1].scatter(vr_rea,vr_tot,c=color,alpha=alpha,s=5,zorder=1)
    ax[0,1].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[0,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[0,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[0,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[0,1].set_aspect('equal', 'box')
    
    vr_rea = vr_rea3
    vr_tot = vr_tot3
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[0,2].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[0,2].scatter(vr_rea,vr_tot,c=color,alpha=alpha,s=5,zorder=1)
    ax[0,2].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[0,2].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[0,2].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[0,2].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[0,2].set_aspect('equal', 'box')
    
    color = color2
    vr_rea = vr_rea1
    vr_tot = vr_tot4
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[1,0].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[1,0].scatter(vr_rea,vr_tot,c=color,alpha=alpha2,s=5,zorder=1,label=llabel2)
    ax[1,0].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[1,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[1,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[1,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[1,0].set_aspect('equal', 'box')
    ax[1,0].legend(loc='lower center')
    
            
    vr_rea = vr_rea2
    vr_tot = vr_tot5
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[1,1].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[1,1].scatter(vr_rea,vr_tot,c=color,alpha=alpha2,s=5,zorder=1)
    ax[1,1].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[1,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[1,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[1,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[1,1].set_aspect('equal', 'box')
    
    vr_rea = vr_rea3
    vr_tot = vr_tot6
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[1,2].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[1,2].scatter(vr_rea,vr_tot,c=color,alpha=alpha2,s=5,zorder=1)
    ax[1,2].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[1,2].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[1,2].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[1,2].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[1,2].set_aspect('equal', 'box')
    
            
    
    plt.subplots_adjust(wspace=0.05,hspace=0.05)
    
    ax[1,0].set_xlabel('variance reduction')
    ax[1,1].set_xlabel('variance reduction')
    ax[1,2].set_xlabel('variance reduction')
    ax[1,0].set_ylabel('estimated var reduction')
    ax[0,0].set_ylabel('estimated var reduction')
    
    ax[0,0].set_title(label1)
    ax[0,1].set_title(label2)
    ax[0,2].set_title(label3)
    
    x_max = np.max(np.hstack([vr_rea1,vr_rea2,vr_tot1,vr_tot2]))
    x_min = np.min(np.hstack([vr_rea1,vr_rea2,vr_tot1,vr_tot2]))
    
   
    ax[0,0].set_xlim(x_min,x_max)
    ax[0,0].set_ylim(x_min,x_max)
    plt.locator_params(axis='y', nbins=4)
    plt.locator_params(axis='x', nbins=4)
    return fig, ax


def vr_scatter_v4(vr_tot1,vr_rea1,vr_tot2,vr_rea2,vr_tot3,vr_tot4,alpha=0.3,alpha2=0.5,color1='blueviolet',color2=plt.cm.viridis(0.8),
                  label1='',label2='',label3='',llabel1='explicit sens',llabel2='implicit sens'):
    """
    Just a 2x2 scatterplot with shared axis and a linear regressions """
    
    fig, ax = plt.subplots(2,2,figsize=(5.5,5.5),sharex='all',sharey='all')
    
    color = color1
    vr_rea = vr_rea1
    vr_tot = vr_tot1
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[0,0].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[0,0].scatter(vr_rea,vr_tot,c=color,alpha=alpha,s=5,zorder=1,label=llabel1)
    ax[0,0].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[0,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[0,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[0,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[0,0].set_aspect('equal', 'box')
    ax[0,0].legend(loc='lower center')
    
            
    vr_rea = vr_rea2
    vr_tot = vr_tot2
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[0,1].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[0,1].scatter(vr_rea,vr_tot,c=color,alpha=alpha,s=5,zorder=1)
    ax[0,1].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[0,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[0,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[0,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[0,1].set_aspect('equal', 'box')
    
    color = color2
    vr_rea = vr_rea1
    vr_tot = vr_tot3
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[1,0].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[1,0].scatter(vr_rea,vr_tot,c=color,alpha=alpha2,s=5,zorder=1,label=llabel2)
    ax[1,0].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[1,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[1,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[1,0].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[1,0].set_aspect('equal', 'box')
    ax[1,0].legend(loc='lower center')
    
            
    vr_rea = vr_rea2
    vr_tot = vr_tot4
    m, b = np.polyfit(vr_rea, vr_tot, 1)
    ax[1,1].plot([-1000,1000],[-1000,1000],'k--',zorder=0)
    ax[1,1].scatter(vr_rea,vr_tot,c=color,alpha=alpha2,s=5,zorder=1)
    ax[1,1].plot(vr_rea, m*np.array(vr_rea) + b,'k',zorder=2) 
    ax[1,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=100,zorder=2)
    ax[1,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='k',s=50,zorder=3)
    ax[1,1].scatter(np.mean(vr_rea),np.mean(vr_tot),c='w',s=10,zorder=4)
    ax[1,1].set_aspect('equal', 'box')
            
    
    plt.subplots_adjust(wspace=0.05,hspace=0.05)
    
    ax[1,0].set_xlabel('variance reduction')
    ax[1,1].set_xlabel('variance reduction')
    ax[1,0].set_ylabel('estimated var reduction')
    ax[0,0].set_ylabel('estimated var reduction')
    
    ax[0,0].set_title(label1)
    ax[0,1].set_title(label2)
    
    x_max = np.max(np.hstack([vr_rea1,vr_rea2,vr_tot1,vr_tot2]))
    x_min = np.min(np.hstack([vr_rea1,vr_rea2,vr_tot1,vr_tot2]))
    
   
    ax[0,0].set_xlim(x_min,x_max)
    ax[0,0].set_ylim(x_min,x_max)
    plt.locator_params(axis='y', nbins=4)
    plt.locator_params(axis='x', nbins=4)
    return fig, ax