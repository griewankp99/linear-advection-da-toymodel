#!/usr/bin/env python
#collection of all functions which determine the model setup and time evolution

import numpy as np

def set_model_constants_22(nx=300,dx=100,u=10,u_std=0,dhdt=0,dhdt_std=0,h_init_std = 3000, init_func='gaus',sine_init=[1],truth_seed=0):
    """
    Sets all the constants used to describe the model. The 2022 version (_22) is only new in that a truth seed is added so that 
    truth perturbations can be added systematically, but that hasn't been tested yet anyway.

    """
    const = {}
    
    #grid
    const["nx"] = nx
    const["dx"] = dx
    const["x_grid"]  = np.arange(nx)*dx
    
    #properties of truth wave
    const["h_init_std"]      = h_init_std  # width of the initial gaussian distribution, or number of sine waves  
    const["sine_init"]       = sine_init  # width of the initial gaussian distribution, or number of sine waves  
    const["init_func"]       = init_func   # currently a gaussian ('gaus') or a sine wave ('sine') are available
    const["u_ref"]           = u           # reference u around which ensemble is generated, truth can 
    const["u_std_truth"]     = u_std       # standard deviation of u for truth 
    const["dhdt_ref"]        = dhdt        # reference dhdt around which ensemble is generated
    const["dhdt_std_truth"]  = dhdt_std   # standard deviation of dhdt for truth
    const["truth_seed"]      = truth_seed   # added to seed used to generate truth deviations
    
    #model
    const["model"] = 'LA'

    return const

def set_model_constants(nx=101,dx=100,u=2.33333333,u_std=0,dhdt=0,dhdt_std=0,h_init_std = 500, init_func='gaus',sine_init=[1]):
    """
    Sets all the constants used to describe the model.
    Currently has only gaussian initial conditions.

    ToDo: Add different initial conditions as an option. 
    """
    const = {}
    
    #grid
    const["nx"] = nx
    const["dx"] = dx
    const["x_grid"]  = np.arange(nx)*dx
    
    #properties of truth wave
    const["h_init_std"]      = h_init_std  # width of the initial gaussian distribution, or number of sine waves  
    const["sine_init"]       = sine_init  # width of the initial gaussian distribution, or number of sine waves  
    const["init_func"]       = init_func   # currently a gaussian ('gaus') or a sine wave ('sine') are available
    const["u_ref"]           = u           # reference u around which ensemble is generated, truth can 
    const["u_std_truth"]     = u_std       # standard deviation of u for truth 
    const["dhdt_ref"]        = dhdt        # reference dhdt around which ensemble is generated
    const["dhdt_std_truth"]  = dhdt_std   # standard deviation of dhdt for truth
    
    #model
    const["model"] = 'LA'

    return const

def gaussian_initial_condition(x,sig):
    """
    Generates a gaussian which is commonly used for the initial conditions. 
    It always uses a mean of zero, and then mirrors values along the x axis to account for periodic boundary conditions.
    This is not very flexible, and also not super clean because it just assumes that the number of x grid points is even
    But since it works making it nice is very far down the priority list. 
    """
    mu = 0
    y = np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))
    half_idx = int(x.shape[0]/2)
    y[-half_idx:] = y[half_idx-1::-1]
    
    return y 

def linear_advection_model(y,u,dydt,dx,dt,nt):
    """ 
    This is a simple wrapper for the advection and growth routines which make up the time evolution of the model. 
    """
    for n in range(nt):
        y = semi_lagrangian_advection(y,dx,u,dt)
        y = linear_growth(y,dydt,dt)
    
    return y


def semi_lagrangian_advection(y,dx,u,dt):
    """
    Advects a 1D field for a given time difference and speed. 
    A linear polation is applied between the nearest grid points. Getting the periodic boundary right is a bit of annoying book keeping.
    Works by calculating the fractional displacement and the integer displacement. 
    Only works with a constant dx.
    """
    dx_frac = u*dt/dx-np.floor(u*dt/dx)
    dx_int = int(np.floor(u*dt/dx))
 
    ##initial quality check, shouldn't be needed anymore
    #if np.abs(u*dt - (dx_frac+dx_int)*dx)>0.00001:
    #    print(u,u*dt,(dx_frac+dx_int)*dx)
    
    if abs(dx_int)>len(y):
        #this is what to do if things move through the whole domain in one step
        if dx_int >0 :
            dx_int = dx_int - int(dx_int/float(len(y)))*len(y)
        if dx_int<0:
            dx_int = dx_int + int(-dx_int/float(len(y)))*len(y)
            
    
    y = np.hstack([(1-dx_frac)*y[0]+(dx_frac)*y[-1],(1-dx_frac)*y[1:]+(dx_frac)*y[:-1]])
    y = np.hstack([y[-dx_int:],y[:-dx_int]])
    return y


def linear_growth(y,dydt,dt):
    """
    Applies linear growth rate. Very highly advanced math.  
    """
    return y + dydt*dt
