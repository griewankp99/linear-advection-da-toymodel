#!/usr/bin/env python

# This is a collection of functions that do not really fit in the model, data assimilation, or plot files. 

import numpy as np

def reflectance_simulator(h,h_c=0.5,window=7,clear_sky=0.3,cloud=0.7):
    """
    calculates the "reflectance" which is a fixed clear sky value where h is below h_c, and a cloud  value where h>h_c.
    As a spatial averaging defined by window, with the total width over the averaging being 2*window+1
    """
    c_binary = h*0.0+clear_sky
    c_binary[h>h_c]=cloud
    nx =h.shape[0] 
    index = np.arange(nx)
    dim = len(h.shape)
    cf= c_binary * 1.
    if window>0 and dim==1:
        for i in range(window):
            cf += c_binary[(index-i) % nx]
            cf += c_binary[(index+i) % nx]
        
    if window>0 and dim==2:
        for i in range(window):
            cf = cf+ c_binary[(index-i) % nx,:]
            cf = cf+ c_binary[(index+i) % nx,:]
    cf = cf/(1.+window*2)
        
    
    return cf 


def cloud_simulator(h,h_c=0.5,window=7):
    """
    First satelitite observation operator. everything above h_c is 1, below is zero, and than an averaging window is applied. The width is defined by window, with the total width over the averaging being 2*window+1.
    
    
    """
    c_binary = h*0.0
    c_binary[h>h_c]=1.
    nx =h.shape[0] 
    index = np.arange(nx)
    dim = len(h.shape)
    cf= c_binary * 1.
    if window>0 and dim==1:
        for i in range(window):
            cf += c_binary[(index-i) % nx]
            cf += c_binary[(index+i) % nx]
        
    if window>0 and dim==2:
        for i in range(window):
            cf = cf+ c_binary[(index-i) % nx,:]
            cf = cf+ c_binary[(index+i) % nx,:]
    cf = cf/(1.+window*2)
        
    
    return cf 